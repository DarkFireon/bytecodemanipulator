import controllers.components.Engine;
import controllers.components.StaticStarter;

import java.util.Scanner;

public class Main {

    //program w zaleznosci moze byc uzyty w dwóch werjsach.
    // Albo z czytaniem wszystkich polecen z cmd (dynamicznie) lub jednorazowy argumentami wywołania  (statyczny)

    //Aby zmienic na dynamiczny działanie programu ustawic STATIC_PROGRAM na false;
    public static final boolean STATIC_PROGRAM = true ;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Engine engine = new Engine();
        String command = null;

        if (STATIC_PROGRAM) {
            StaticStarter.execute(args);
        } else {
            do {
                command = sc.nextLine();
                engine.execute(command);
            } while (sc.hasNext());
        }

    }
}


