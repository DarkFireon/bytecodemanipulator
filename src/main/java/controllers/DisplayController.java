package controllers;

import javassist.*;

import java.util.ArrayList;

import static controllers.JarController.classes;
import static controllers.JarController.pool;

public class DisplayController extends Controller {

    public void execute(String command) throws NotFoundException {
        if (command.equals("packages")) {
            getPackages();
            return;
        }
        if (command.equals("classes")) {
            showClasses();
            return;
        }
        if (command.split(" ").length != 2) {
            System.out.println("Error, bledne dane wejsciowe");
            return;
        }
        String displayType = command.split(" ")[0];
        String className = command.split(" ")[1];

        if (displayType.equals("methods")) {
            showMethods(className);
            return;
        }
        if (displayType.equals("fields")) {
            showFields(className);
            return;
        }
        if (displayType.equals("ctors")) {
            showCons(className);
        }
    }

    private void showClasses()  {
        for (Class clz : classes.keySet()) {
            if(!clz.getSimpleName().equals(""))
            System.out.println(clz.getSimpleName());
        }
    }

    void getPackages() {
        ArrayList<String> packages = new ArrayList<>();
        String packagee;
        for (Class clz : JarController.classes.keySet()) {
            packagee=clz.getName().substring(0, clz.getName().lastIndexOf("."));
            if (!packages.contains(packagee)) {
                packages.add(packagee);
            }
        }
        for(String s : AddController.packages){
            packagee = s.replace("/",".");
            packagee = packagee.substring(0, packagee.length() - 1);
            if (!packages.contains(packagee)) {
                packages.add(packagee);
            }
        }
        packages.forEach(System.out::println);
    }

    void showMethods(String className) throws NotFoundException {
        CtClass ct = pool.get(className);
        for (CtMethod m : ct.getDeclaredMethods()) {
            System.out.println(m.getReturnType().getName() + " " + m.getLongName());
        }
    }

    void showFields(String className) throws NotFoundException {
        CtClass ct = pool.get(className);
        for (CtField m : ct.getDeclaredFields()) {
            System.out.println(m.getName());
        }
    }

    void showCons(String className) throws NotFoundException {
        CtClass ct = pool.get(className);
        for (CtConstructor c : ct.getDeclaredConstructors()) {
            System.out.println(c.getLongName());
        }
    }

}
