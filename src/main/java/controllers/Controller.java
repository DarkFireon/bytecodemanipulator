package controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Controller {

    public abstract void execute(String command) throws Exception;

    String getTextFromFile(String path) throws IOException {

        StringBuilder text = new StringBuilder();
        File file = new File(path);
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null)
            text.append(st);

        return text.toString();
    }



}
