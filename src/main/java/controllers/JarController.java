package controllers;


import javassist.ClassPool;
import javassist.NotFoundException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarController extends Controller {

    static public Map<Class<?>, Boolean> classes;

    static ArrayList<String> files;
    private JarFile jar;
    static public ClassPool pool;

    public JarController() {
    }

    public void execute(String name) throws IOException, NotFoundException {
        files = new ArrayList<>();
        classes = new HashMap<>();

        URL jarUrl;

        try {
            jar = new JarFile(name);
            jarUrl = new File(name).toURI().toURL();
        } catch (IOException e) {
            System.out.println("Cos sie pliki pomylily");
            return;
        }

        URLClassLoader loader = new URLClassLoader(new URL[]{jarUrl});

        for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements(); ) {
            JarEntry entry = entries.nextElement();
            String file = entry.getName();
            if (file.endsWith("MANIFEST.MF")) {
                continue;
            }
            if (!file.substring(file.lastIndexOf("\\") + 1).trim().contains(".")) {
                continue;
            }

            if (file.endsWith(".class")) {
                String classname =
                        file.replace('/', '.').substring(0, file.length() - 6);
                try {
                    Class<?> c = loader.loadClass(classname);
                    classes.put(c, false);
                } catch (Throwable e) {
                    //System.out.println("WARNING: failed to instantiate " + classname + " from " + file);
                }
            } else {
                files.add(file);
            }
        }
        System.out.println("Plik jar zostal zamieniony");
        pool = new ClassPool(ClassPool.getDefault());
        pool.appendClassPath(name);
    }

}
