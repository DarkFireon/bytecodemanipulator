package controllers;
import controllers.components.Engine;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ScriptController extends Controller {
    private Engine engine;

    public ScriptController(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void execute(String command) throws Exception {

        try (Stream<String> stream = Files.lines(Paths.get(command))) {
            stream.forEach(engine::execute);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
