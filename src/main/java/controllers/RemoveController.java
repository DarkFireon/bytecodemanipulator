package controllers;

import javassist.*;
import javassist.bytecode.Descriptor;

import java.util.ArrayList;
import java.util.List;

import static controllers.JarController.pool;

public class RemoveController extends Controller {

    public void execute(String command) throws Exception {

        if (command.split(" ").length < 2) {
            System.out.println("Error, bledne dane wejsciowe");
            return;
        }
        String updateType = command.split(" ")[0];
        String objectName = command.substring(updateType.length() + 1);

        if (updateType.equals("package")) {
            removePackage(objectName);
            return;
        }
        if (updateType.equals("class") || updateType.equals("interface")) {
            removeClass(objectName);
            return;
        }
        if (updateType.equals("method")) {
            removeMethod(objectName);
            return;
        }
        if (updateType.equals("ctor")) {
            removeConstructor(objectName);
            return;
        }
        if (updateType.equals("field")) {
            removeField(objectName);
        }
    }

    private void removePackage(String objectName) throws Exception {
        boolean bool = AddController.packages.remove(objectName.replace(".", "/") + "/");

        if (!bool) {
            throw new Exception("Nie ma takiego pakietu mozliwego do usuniecia");
        }
        List<Class> operatedList = new ArrayList<>();
        JarController.classes.keySet().stream().
                filter(clz -> clz.getName().startsWith(objectName))
                .forEach(clz -> {
                    operatedList.add(clz);
                });
        JarController.classes.keySet().removeAll(operatedList);
    }
    private void removeField(String objectName) throws NotFoundException {
        String className = objectName.split(" ")[0];
        String fieldName = objectName.split(" ")[1];
        CtField ctField = pool.get(className).getField(fieldName);
        pool.get(className).removeField(ctField);
        System.out.println("usunieto field " + fieldName);
    }

    private void removeConstructor(String objectName) throws NotFoundException {
        String className = objectName.substring(0, objectName.indexOf('('));
        CtClass[] classes = getParams(objectName);
        CtConstructor ctCons = pool.get(className).getConstructor(Descriptor.ofConstructor(classes));
        pool.get(className).removeConstructor(ctCons);
        System.out.println("usunieto konstruktor ");
    }

    private void removeMethod(String objectName) throws NotFoundException {
        String method = objectName.substring(0, objectName.indexOf('('));
        String className = method.substring(0, method.lastIndexOf('.'));
        method = method.substring(method.lastIndexOf('.') + 1);
        CtClass[] classes=getParams(objectName);;
        CtMethod ctMethod = pool.get(className).getDeclaredMethod(method, classes);
        pool.get(className).removeMethod(ctMethod);
    }

    private CtClass[] getParams(String objectName) throws NotFoundException {
        String[] params = objectName.substring(objectName.indexOf('(') + 1, objectName.indexOf(')')).split(",");
        CtClass[] classes = new CtClass[params.length];
        for (int i = 0; i < params.length; i++) {
            classes[i] = pool.get(params[i]);
        }
        return classes;
    }

    private void removeClass(String className) throws NotFoundException {
        Class clazz = JarController.classes.keySet().stream()
                .filter(clz -> className.equals(clz.getName()))
                .findAny()
                .orElse(null);
        if (clazz == null) {
            System.out.println("Nie ma takiej klasy");
            return;
        }
        boolean removed = JarController.classes.remove(clazz, true);
        if (removed) {
            System.out.println("usunieto");
        } else {
            System.out.println("nie mozesz usunac tej klasy");
        }
    }
}
