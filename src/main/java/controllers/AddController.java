package controllers;

import javassist.*;

import java.io.IOException;
import java.util.ArrayList;

import static controllers.JarController.classes;
import static controllers.JarController.pool;

public class AddController extends Controller {
    static ArrayList<String> packages = new ArrayList<>();

    public AddController() {
    }
    @Override
    public void execute(String command) throws IOException, NotFoundException, CannotCompileException, IllegalAccessException, NoSuchFieldException {

        if (command.split(" ").length < 2) {
            System.out.println("Error, bledne dane wejsciowe");
            return;
        }
        String addType = command.split(" ")[0];
        String objectName = command.substring(addType.length() + 1);

        switch (addType) {
            case "package":
                addPackage(objectName);
                break;
            case "class":
                addClass(objectName);
                break;
            case "interface":
                addInterface(objectName);
                break;
            case "method":
                addMethod(objectName);
                break;
            case "before-method":
                addToMethod(objectName, false);
                break;
            case "after-method":
                addToMethod(objectName, true);
                break;
            case "ctor":
                addConstructor(objectName);
                break;
            case "field":
                addField(objectName);
        }
    }

    private void addToMethod(String objectName, Boolean after) throws NotFoundException, CannotCompileException, IOException {
        String clazzName = objectName.substring(0, objectName.lastIndexOf("("));
        clazzName = clazzName.substring(0, clazzName.lastIndexOf("."));
        CtClass clazz = pool.get(clazzName);

        String path = objectName.split(" ")[1];
        String text = getTextFromFile(path);

        CtMethod ctMethod = null;
        for (CtMethod ct : clazz.getDeclaredMethods()) {
            if (ct.getLongName().equals(objectName.split(" ")[0]))
                ctMethod = ct;
        }
        if (after) {
            ctMethod.insertAfter(text);
        } else {
            ctMethod.insertBefore(text);
        }
    }

    private void addPackage(String objectName) {
        String packagee = objectName.replace(".", "/") + "/";
        if (!packages.contains(packagee))
            packages.add(objectName.replace(".", "/") + "/");
    }

    private void addField(String objectName) throws NotFoundException, CannotCompileException, IllegalAccessException, NoSuchFieldException {
        int length = objectName.split(" ").length;
        String className = objectName.split(" ")[0];
        String fieldType = objectName.split(" ")[1];
        String fieldName = objectName.split(" ")[2];
        CtField f1 = null;
        CtClass clazz = pool.get(className);

        if (length == 3) {
            f1 = CtField.make(fieldType + " " + fieldName + ";", clazz);
        } else if (length == 4) {
            String value = objectName.split(" ")[3];

            if (value.equals("static")) {
                f1 = CtField.make("public static " + fieldType + " " + fieldName + ";", clazz);
            } else
                f1 = CtField.make(fieldType + " " + fieldName + " = " + value + ";", clazz);
        } else if (length == 5) {
            String value = objectName.split(" ")[3];
            boolean isItStatic = objectName.split(" ")[4].equals("static");
            if (isItStatic) {
                f1 = CtField.make("public static " + fieldType + " " + fieldName + " = " + value + ";", clazz);
            }
        }
        clazz.addField(f1);
    }

    private void addConstructor(String objectName) throws NotFoundException, CannotCompileException {
        String className = objectName.split(" ")[0];
        String[] params = objectName.substring(objectName.indexOf(" ") + 1).split(" ");

        CtClass[] ctClasses = new CtClass[params.length];
        for (int i = 0; i < params.length; i++) {
            ctClasses[i] = pool.get(params[i]);
        }
        CtClass clazz = pool.get(className);
        CtConstructor c = new CtConstructor(ctClasses, clazz);
        pool.get(className).addConstructor(c);
    }

    private void addMethod(String objectName) throws NotFoundException, CannotCompileException, IOException {
        String[] params = objectName.split(" ");
        String className = params[0];
        String returnType = params[1];
        String name = params[params.length - 1];
        String functionParams = "";
        for (int i = 2; i < params.length - 1; i++) {
            functionParams += params[i];
            functionParams += " ";
        }
        CtClass clazz = pool.get(className);
        String function = "public " + returnType + " " + name + "(" + functionParams + ")" + "{ return null;}";
        clazz.defrost();
        CtMethod happyMethod = CtMethod.make(function, clazz);
        pool.get(className).addMethod(happyMethod);
    }

    private void addClass(String objectName) throws CannotCompileException, NotFoundException {
        String packageName = objectName.substring(0, objectName.lastIndexOf("."));
        Boolean newPackage = classes.keySet().stream().
                anyMatch(clz -> (clz.getName().substring(0, clz.getName().lastIndexOf(".")).equals(packageName)));
        if (!newPackage) {
            addPackage(packageName);
        }
        CtClass ctClass = pool.makeClass(objectName);
        if (objectName.contains("Runnable")) {
            ctClass.addInterface(pool.get("java.lang.Runnable"));
            CtMethod method = CtNewMethod.make("public void run() { System.out.println(\"hello!!!!\"); }", ctClass);
            ctClass.addMethod(method);
        }
        classes.put(ctClass.toClass(), true);
    }

    private void addInterface(String objectName) throws CannotCompileException {
        CtClass ctClass = pool.makeInterface(objectName);
        classes.put(ctClass.toClass(), true);
    }
}
