package controllers.components;
import java.util.ArrayList;
import java.util.Arrays;

public class StaticStarter {
    public static void execute(String[] args) {
        ArrayList<String> arguments = new ArrayList<>(Arrays.asList(args));
        Engine engine = new Engine();
        String command = null;

        if (arguments.contains("--i")) {
            String jarFile = arguments.get(arguments.indexOf("--i") + 1);
            arguments.remove((arguments.indexOf("--i") + 1));
            arguments.remove("--i");
            engine.execute("--i " + jarFile);
        }
        if (arguments.contains("--o")) {
            command = arguments.get(arguments.indexOf("--o") + 1);
            arguments.remove((arguments.indexOf("--o") + 1));
            arguments.remove("--o");
        }
        for (int i = 0; i < arguments.size(); i++) {

            if (arguments.get(i).equals("--list-packages") || arguments.get(i).equals("--list-classes")) {
                engine.execute(arguments.get(i));
            } else {
                engine.execute(arguments.get(i) + " " + arguments.get(i + 1));
                i++;
            }
        }
        if (command != null)
            engine.execute("--o " + command);
    }

}
