package controllers.components;

import controllers.*;
import javassist.CannotCompileException;
import javassist.NotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Engine {
    private Map<String, Controller> controllerMap = new HashMap<>();

    public  Engine() {
        ScriptController scriptController = new ScriptController(this);
        JarController jarController = new JarController();
        SavingController savingController = new SavingController();
        DisplayController displayController = new DisplayController();
        RemoveController updateController = new RemoveController();
        AddController addController = new AddController();
        SetController setController = new SetController();
        controllerMap.put("--script ", scriptController);
        controllerMap.put("set-", setController);
        controllerMap.put("--i ", jarController);
        controllerMap.put("--list-", displayController);
        controllerMap.put("--o ", savingController);
        controllerMap.put("add-", addController);
        controllerMap.put("remove-", updateController);
    }

    public void execute(String command) {
        if (command.equals(""))
            return;
        String functionType = checkCommandAndReturnType(command);
        if (functionType == null) {
            System.out.println("Error");
            return;
        }

        Controller controller = controllerMap.get(functionType);
        String arguments = extractArguments(command, functionType);

        try {
            controller.execute(arguments);
        } catch (IOException | NotFoundException | CannotCompileException | NullPointerException e) {
            e.printStackTrace();
            System.out.println("Error");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String checkCommandAndReturnType(String command) {
        //Otrzymanie wartosci
        Pattern pattern = Pattern.compile("((--i )|(--o )|(--list-)|(add-)|(remove-)|(set-)|(--script )).*");

        Matcher matcher = pattern.matcher(command);
        if (!matcher.matches()) {
            return null;
        }
        //Jaka jest/moze byc to funkcja
        pattern = Pattern.compile("(--i )|(--o )|(--list-)|(add-)|(remove-)|(set-)|(--script )");
        matcher = pattern.matcher(command);
        matcher.find();
        return matcher.group();
    }
    private String extractArguments(String command, String functionType) {
        Pattern pattern = Pattern.compile("(?<=" + functionType + ").*");
        Matcher matcher = pattern.matcher(command);
        matcher.find();
        return matcher.group();
    }
}