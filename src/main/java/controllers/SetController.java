package controllers;
import javassist.*;
import java.io.IOException;
import static controllers.JarController.pool;

public class SetController extends Controller {

    @Override
    public void execute(String command) throws IOException, NotFoundException, CannotCompileException {
        if (command.split(" ").length < 2) {
            System.out.println("Error, bledne dane wejsciowe");
            return;
        }
        String setType = command.split(" ")[0];
        String objectName = command.substring(setType.length() + 1);
        if (setType.equals("method-body")) {
            setMethodBody(objectName);
            return;
        }
        if (setType.equals("ctor-body")) {
            setConstructorBody(objectName);
        }
    }

    private void setConstructorBody(String objectName) throws NotFoundException, CannotCompileException, NullPointerException, IOException {
        String clazzName = objectName.substring(0, objectName.indexOf("("));
        CtClass clazz = pool.get(clazzName);
        String path = objectName.split(" ")[1];
        String text = getTextFromFile(path);
        CtConstructor ctConstructor = null;
        for (CtConstructor ct : clazz.getDeclaredConstructors()) {
            if (ct.getLongName().equals(objectName.split(" ")[0]))
                ctConstructor = ct;
        }
        ctConstructor.setBody(text);
    }

    private void setMethodBody(String objectName) throws NotFoundException, CannotCompileException, IOException {
        String clazzName = objectName.substring(0, objectName.indexOf("("));
        clazzName = clazzName.substring(0, clazzName.lastIndexOf("."));
        CtClass clazz = pool.get(clazzName);
        clazz.defrost();
        String path = objectName.split(" ")[1];
        String text = getTextFromFile(path);
        CtMethod ctMethod;
        ctMethod = null;
        for (CtMethod ct : clazz.getDeclaredMethods()) {
            if (ct.getLongName().equals(objectName.split(" ")[0]))
                ctMethod = ct;
        }
        ctMethod.setBody(text);
    }
}
