package controllers;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.NotFoundException;

import java.io.*;
import java.net.URL;
import java.util.Calendar;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class SavingController extends Controller {


    public void makeJar(String jarOutputName) throws IOException, NotFoundException {
        String version = "1.0.0";
        String author = "ArturKisicki";
        Manifest manifest = new Manifest();
        Attributes global = manifest.getMainAttributes();
        global.put(Attributes.Name.MANIFEST_VERSION, version);
        global.put(new Attributes.Name("Created-By"), author);
        global.put(Attributes.Name.MAIN_CLASS, "com.diamond.iain.javagame.Game");
        JarOutputStream target = null;
        try {
            File jarFile = new File(jarOutputName);
            OutputStream os = new FileOutputStream(jarFile);
            target = new JarOutputStream(os, manifest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int len = 0;
        byte[] buffer = new byte[1024];
        String jarName = "invaders.jar";
        for (String name : JarController.files) {

            JarEntry je = new JarEntry(name);
            File file = new File(jarName + "!/" + name);
            je.setComment("Craeting Jar");
            je.setTime(Calendar.getInstance().getTimeInMillis());
            target.putNextEntry(je);
            URL url = file.toURI().toURL();
            url = new URL("jar:" + url.toString());
            InputStream is = url.openStream();
            while ((len = is.read(buffer, 0, buffer.length)) != -1) {
                target.write(buffer, 0, len);
            }
            is.close();
            target.closeEntry();
        }
        for (String s : AddController.packages) {
            JarEntry je = new JarEntry(s);
            target.putNextEntry(je);
        }
        for (Class clz : JarController.classes.keySet()) {
            String entry = clz.getName().replace('.', '/');
            entry += ".class";
            JarEntry je = new JarEntry(entry);
            je.setComment("Craeting Jar");
            je.setTime(Calendar.getInstance().getTimeInMillis());
            target.putNextEntry(je);
            CtClass ctClass = JarController.pool.get(clz.getName());
            try {
                target.write(ctClass.toBytecode());
            } catch (CannotCompileException e) {
                e.printStackTrace();
            }
            target.closeEntry();
        }
        target.close();
        System.out.println("Plik zostal zapisany");
        target.close();
    }
    @Override
    public void execute(String command) throws IOException, NotFoundException {
        makeJar(command);
    }
}
